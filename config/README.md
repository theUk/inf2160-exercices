# Configuration

Ce dossier contient des fichiers de configuration minimaux qui pourraient vous
être utiles pour reproduire un environnement de développement similaire à celui
que j'utilise en classe.

## Fichier `.vimrc`

Il s'agit d'un fichier de configuration de base pour Vim. Noter que pour le
faire fonctionner correctement, il est important d'installer
[Vim-plug](https://github.com/junegunn/vim-plug). Il est important d'enrichir
ce fichier selon vos besoins, mais il constitue un bon point de départ si vous
n'êtes pas familier avec ce logiciel.

## Fichier `.tmux.conf`

Il s'agit aussi d'un fichier de configuration de base pour le multiplexeur de
terminal [Tmux](https://github.com/tmux/tmux/wiki). Je l'utilise principalement
pour ouvrir plusieurs fenêtres dans un même terminal.

## Fichier `.profile`

Quelques raccourcis que j'ajoute à mon fichier `.profile` (sous Linux, le
fichier devrait plutôt s'appeler `.bashrc`) pour modifier l'invite de commande,
afin qu'elle indique la branche Git sur laquelle je me trouve actuellement.  Il
indique également que Vim est l'éditeur à utiliser par défaut par Git.
