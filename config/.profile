# Colors in the terminal
function parse_git_branch_and_add_brackets {
  git branch --no-color 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/\ \[\1\]/'
  }
export LSCOLORS=CxdxxxxxExxxxxExExCxCx
PS1="\n\e[36m\$(parse_git_branch_and_add_brackets) \e[32;1m\u\e[0m \e[33;1m[\w]\e[0m\n $ "

# Giteditor
export GIT_EDITOR=`which vim`
