# Paradigmes de programmation

Ce dépôt contient les énoncés des laboratoires du cours INF2160 Paradigmes de
programmation, enseigné à l'UQAM.

* [Quelques fichiers de configuration](config/README.md)
* [Labo 1](labo01/README.md)
* [Labo 2](labo02/README.md)
* [Labo 3](labo03/README.md)
