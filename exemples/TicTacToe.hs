{-
Module      : Tic-Tac-Toe
Description : A simple tic-tac-toe game
Copyright   : (c) Alexandre Blondin Massé
License     : GPL-3
Maintainer  : blondin_masse.alexandre@uqam.ca
Stability   : experimental

This module provides basic functionalities for simulating a tic-tac-toe game.
 -}

-- | Une case de la grille peut être soit occupée par X, par O ou vide.
data Case = X | O | Vide
    deriving (Show, Eq)

-- | Retourne la valeur inverse d'une case
caseInverse :: Case -> Case
caseInverse X = O
caseInverse O = X
caseInverse Vide = Vide

-- | Une grille de tic-tac-toe 3 x 3
type Grille = ((Case, Case, Case),
               (Case, Case, Case),
               (Case, Case, Case))

-- | Retourne une grille vide (toutes les cases sont vides)
grilleVide :: Grille
grilleVide = ((Vide, Vide, Vide),
              (Vide, Vide, Vide),
              (Vide, Vide, Vide))

-- | Indique si une grille a au moins une case vide
aCaseVide :: Grille -> Bool
aCaseVide ((c11, c12, c13), (c21, c22, c23), (c31, c32, c33))
    = c11 == Vide || c12 == Vide || c13 == Vide ||
      c21 == Vide || c22 == Vide || c23 == Vide ||
      c31 == Vide || c32 == Vide || c33 == Vide

-- | Indique si le joueur X ou O a gagné
estGagnante :: Case -> Grille -> Bool
estGagnante c ((c11, c12, c13), (c21, c22, c23), (c31, c32, c33))
    = (c == c11 && c == c12 && c == c13) ||
      (c == c21 && c == c22 && c == c23) ||
      (c == c31 && c == c32 && c == c33) ||
      (c == c11 && c == c21 && c == c31) ||
      (c == c12 && c == c22 && c == c32) ||
      (c == c13 && c == c23 && c == c33) ||
      (c == c11 && c == c22 && c == c33) ||
      (c == c13 && c == c22 && c == c31)

-- | Joue un coup à l'emplacement indiqué
jouerCoup :: Case -> (Int,Int) -> Grille -> Grille
jouerCoup c ij g
    | c /= Vide = jouerCoup' c ij g
    | otherwise = error "La case ne doit pas être vide"
    where jouerCoup' c (1,1) ((Vide, c2, c3), l2, l3) = ((c, c2, c3), l2, l3)
          jouerCoup' c (1,2) ((c1, Vide, c3), l2, l3) = ((c1, c, c3), l2, l3)
          jouerCoup' c (1,3) ((c1, c2, Vide), l2, l3) = ((c1, c2, c), l2, l3)
          jouerCoup' c (2,1) (l1, (Vide, c2, c3), l3) = (l1, (c, c2, c3), l3)
          jouerCoup' c (2,2) (l1, (c1, Vide, c3), l3) = (l1, (c1, c, c3), l3)
          jouerCoup' c (2,3) (l1, (c1, c2, Vide), l3) = (l1, (c1, c2, c), l3)
          jouerCoup' c (3,1) (l1, l2, (Vide, c2, c3)) = (l1, l2, (c, c2, c3))
          jouerCoup' c (3,2) (l1, l2, (c1, Vide, c3)) = (l1, l2, (c1, c, c3))
          jouerCoup' c (3,3) (l1, l2, (c1, c2, Vide)) = (l1, l2, (c1, c2, c))
          jouerCoup' _ _ _ = error "Coup invalide"

-- | Joue une suite de coups aux emplacements indiqués
jouerCoups :: [(Case,Int,Int)] -> Grille -> Grille
jouerCoups [] g = g
jouerCoups ((c,i,j):ps) g = jouerCoups ps (jouerCoup c (i,j) g)

jouerPartie :: [(Int,Int)] -> Grille
jouerPartie ps =
    let jouerPartie' [] _ g = g
        jouerPartie' ((i,j):ps) c g
            | fini && coupsEpuises     = grille'
            | fini && not coupsEpuises = error "Coups en trop"
            | otherwise                = jouerPartie' ps (caseInverse c) grille'
            where coupsEpuises = null ps
                  grille'      = jouerCoup c (i,j) g
                  fini         = estGagnante c grille'
    in  jouerPartie' ps X grilleVide

-- | Une partie est soit en cours, soit gagnée par X, soit gagnée par O, soit
-- nulle
data EtatPartie = EnCours | XGagne | OGagne | PartieNulle
    deriving (Show)

-- | Retourne l'état d'une grille
etatGrille :: Grille -> EtatPartie
etatGrille g
    | estGagnante X g = XGagne
    | estGagnante O g = OGagne
    | aCaseVide g     = EnCours
    | otherwise       = PartieNulle
