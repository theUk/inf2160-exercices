# Laboratoire 2: Fonctions et type

# 1 - Retour sur les listes

Réimplémentez les fonctions suivantes sur les listes, sans regarder le code
source:
```haskell
head :: [a] -> a
last :: [a] -> a
tail :: [a] -> [a]
init :: [a] -> [a]
null :: [a] -> Bool
length :: [a] -> Int
replicate :: Int -> a -> [a]
take :: Int -> [a] -> [a]
drop :: Int -> [a] -> [a]
map :: (a -> b) -> [a] -> [b]
filter :: (a -> b) -> [a] -> [b]
```
Notez que vous devez les renommer (car elles sont chargées par `Prelude`). La
stratégie conventionnelle consiste à ajouter le caractère prime (`'`) à la fin
pour qu'il n'y ait pas de conflit.

# 2 - Fonctions d'ordre supérieur

Récupérez le fichier [ExercicesFonctions.hs](ExercicesFonctions.hs) disponible
dans ce répertoire.

En utilisant les fonctions suggérées entre parenthèses, répondez aux questions
suivantes. *Remarque*: même s'il est possible de le faire autrement qu'en
utilisant les fonctions suggérées, je vous encourage à respecter les
contraintes, afin de mieux apprendre à utiliser certaines fonctions.

## 2.1 - Multiplication par un scalaire

(`map`, `.`, `(*5)`) Produisez la matrice obtenue de `matriceA` en multipliant
chaque entrée par `5`. Vous devriez donc obtenir
```haskell
[[15,5,-10],[-5,0,10],[20,-5,25]]
```

(`map`, `.`, `(*)`) Déduisez-en l'implémentation d'une fonction
`multiplicationParScalaire` qui multiplie toutes les entrées d'une matrice par
un scalaire donné, dont la signature est
```haskell
multiplicationParScalaire :: Num a => a -> [[a]] -> [[a]]
```

## 2.2 - Somme de deux matrices

(`zipWith`, `(+)`, `.`) Produisez la somme des matrices `matriceA` et
`matriceB`. Le résultat attendu est
```haskell
[[-1,4,2],[0,-6,-1],[8,6,-4]] 
```

(`zipWith`, `(+)`, `.`) Déduisez-en une fonction
```haskell
sommeMatrices :: Num a => [[a]] -> [[a]] -> [[a]]
```
qui calcule la somme de deux matrices.

## 2.3 - Produit scalaire

(`sum`, `zipWith`, `(*)`) Définissez une fonction qui calcule le produit
scalaire de deux listes (pas besoin de valider si les longueurs sont égales,
vous pouvez supposer que c'est le cas). La signature est
```haskell
produitScalaire :: Num a => [a] -> [a] -> a
```
On s'attend à ce que le résultat soit le suivant
```haskell
Prelude> produitScalaire [1,2,3] [4,5,6]
32
Prelude> produitScalaire [-1,-3,2] [0,2,4]
2
```

## 2.4 - Produit matriciel

(`produitScalaire`, `transpose`) Définissez une fonction qui calcule le produit
de deux matrices, dont la signature est
```haskell
produitMatrices :: Num a => [[a]] -> [[a]] -> [[a]]
```
Il y a une façon élégante de faire en observant que le produit de deux matrices
peut être vu comme le produit scalaire de chaque ligne de la première matrice
avec chaque colonne de la deuxième matrice. *Note*: La compréhension de liste
pourrait aussi vous être utile.

# 3 - Jeu de tic-tac-toe

Pour cet exercice, vous devez récupérer le fichier
[TicTacToe.hs](../exemples/TicTacToe.hs) disponible dans ce dépôt.

## 3.1 - Affichage d'une grille

Implémentez une fonction qui permet d'afficher une grille, dont la signature
est
```haskell
showGrille :: Grille -> String
```
qui affiche la grille sous format plus agréable.

Nous aimerions avoir le comportement suivant:
```haskell
Prelude> putStrLn $ showGrille ((X,O,Vide),(X,Vide,O),(X,Vide,Vide))
X | O |  
--+---+--
X |   | O
--+---+--
X |   |  
```
*Note*: la fonction `putStrLn` permet d'afficher une chaîne de caractère (donc
les guillemets n'y sont plus, et les `\n` sont interprétés comme des retours à
la ligne).

## 3.2 - Réimplémentation de `jouerCoups`

Modifiez l'implémentation de la fonction `jouerCoups` pour qu'elle fasse appel
à la fonction `foldr` (ou `foldl`).

## 3.3 - Réimplémentation de `jouerPartie`

Cet exercice est plus difficile, mais permet de bien comprendre la puissance
des fonctions d'ordre supérieur.

Modifiez l'implémentation de la fonction `jouerPartie` pour qu'elle fasse appel
à la fonction `jouerCoups`. Il pourrait être intéressant d'utiliser la fonction
`foldr` (ou `foldl`), la fonction `zip3` et l'expression `cycle [X,O]`.

